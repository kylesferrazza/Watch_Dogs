﻿using UnityEngine;
using System.Collections;

public class gameCore : MonoBehaviour {
	public Object Dog1;
	public Object Dog2;
	public Object Dog3;
	public Object dogToSpawn;
	public Vector3 spawnLoc;
	public Quaternion spawnRot;
	public float randX;

	void Start () {
		respawnDog (1);
		respawnDog (2);
		respawnDog (3);
	}
	void Update () {
	}
	void respawnDog(float dogNumber) {
		getRandX ();
		spawnLoc.Set(randX, 15, 0);
		spawnRot = Random.rotation;
		if (dogNumber == 1) {
			dogToSpawn = Dog1;
			//spawnRot = Quaternion.Euler(270.0f, 270.0f, 0.0f);
		}
		else if (dogNumber == 2) {
			dogToSpawn = Dog2;
			//spawnRot = Quaternion.Euler(270.0f, 180.0f, 0.0f);
		}
		else if (dogNumber == 3) {
			dogToSpawn = Dog3;
			//spawnRot = Quaternion.Euler(270.0f, 180.0f, 0.0f);
		}
		GameObject spawnedDog = (GameObject) Instantiate (dogToSpawn, spawnLoc, spawnRot);
		spawnedDog.rigidbody.drag = Random.Range (0f,25f) / 10f;
	}
	void getRandX () {
		randX = Random.Range (-6, 6);
	}
}

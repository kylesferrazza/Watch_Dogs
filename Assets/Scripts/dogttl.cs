﻿using UnityEngine;
using System.Collections;

public class dogttl : MonoBehaviour {
	public GameObject dogSpawn;
	public float dogNumber;
	// Use this for initialization
	void Start () {
		StartCoroutine ("spawn");
	}
	//move spawning coroutine to gamecore.. make it random
	public IEnumerator spawn () {
		yield return new WaitForSeconds (Random.Range(10, 250) / 100);
		dogSpawn = GameObject.Find("dogSpawn");
		dogSpawn.SendMessage("respawnDog", dogNumber, SendMessageOptions.DontRequireReceiver);
	}
}
